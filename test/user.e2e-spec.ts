import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { UserRegisterRequestDto, UserRegisterResponseDto, UserLoginRequestDto, UserLoginResponseDto } from '../src/user/dto';
import { ConfigService } from '../src/config';

describe('UserController (e2e)', () => {
  let app;

  beforeEach(async () => {

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
    .overrideProvider(ConfigService)
    .useValue({
      JWT_SECRET: 'jwt-secret',
    })
    .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/user/register (POST)', () => {

    const req: UserRegisterRequestDto = {
      name: 'piotr',
      email: 'piotr@myflow.pl',
      password: '123',
    };

    const res: UserRegisterResponseDto = {
      user: {
        id: expect.any(Number),
        name: 'piotr',
        email: 'piotr@myflow.pl',
      },
    };
    return request(app.getHttpServer())
      .post('/user/register')
      .send(req)
      .expect(201)
      .then(r => {
        expect(r.body).toMatchObject(res);
      });
  });


  // logowanie

  it('/user/login SUCCESS', () => {

    const req: UserLoginRequestDto = {
      email: 'piotr@myflow.pl',
      password: '123',
    };

    const resBody: UserLoginResponseDto = {
      token: expect.any(String),
      user: {
        id: expect.any(Number),
        name: 'Piotr',
        email: 'piotr@myflow.pl',
      },
    };

    return request(app.getHttpServer())
      .post('/user/login')
      .send(req)
      .expect(201)
      .then(res => {
        expect(res.body).toMatchObject(resBody);
      });
  });

  it('/user/login ERROR', () => {
    const req: UserLoginRequestDto = {
      email: 'piotr@myflow.pl',
      password: '0004',
    };

    return request(app.getHttpServer())
      .post('/user/login')
      .send(req)
      .expect(422);
  });

});
