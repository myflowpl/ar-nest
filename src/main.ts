require('dotenv').config();

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ExpressAdapter, NestExpressApplication } from '@nestjs/platform-express';

import express = require('../express/server');
import { ConfigService } from './config';
import { join } from 'path';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, new ExpressAdapter(express));
  app.setGlobalPrefix('api');
  app.enableCors();

  app.useStaticAssets(join(__dirname, '..', 'assets'));

  const config = app.get<ConfigService>(ConfigService);

  const options = new DocumentBuilder()
    .setTitle('Nest API Example')
    .setDescription('Przykładowy projekt w Node.js i TypeScript')
    .setVersion('1.0')
    .addTag('user')
    .setBasePath('api')
    .addBearerAuth(config.TOKEN_HEADER_NAME, 'header')
    .build();
    
  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('docs', app, document);

  await app.listen(3000);
}
bootstrap();
