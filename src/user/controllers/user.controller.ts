import { Controller, Post, Body, Get, Request, UseGuards, HttpException, HttpStatus, UsePipes, ValidationPipe, Param } from '@nestjs/common';
import { UserService } from '../services/user.service';
import { ApiCreatedResponse, ApiBearerAuth, ApiImplicitParam } from '@nestjs/swagger';
import { UserRegisterResponseDto, UserRegisterRequestDto, UserLoginRequestDto, UserLoginResponseDto } from '../dto';
import { User } from '../decorators/user.decorator';
import { Roles } from '../decorators/roles.decorator';
import { UserRole, UserModel } from '../models';
import { AuthGuard } from '../guards/auth.guard';
import { AuthService } from '../services';
import { UserByIdPipe } from '../pipes/user-by-id.pipe';

@Controller('user')
export class UserController {

  
  constructor(
    private userService: UserService,
    private authService: AuthService,
  ) {}

  @Post('login')
  @UsePipes(new ValidationPipe({ transform: true }))
  async login(@Body() credentials: UserLoginRequestDto): Promise<UserLoginResponseDto> {

    const user = await this.userService.findByCredentials(credentials.email, credentials.password);

    if (!user) {
      throw new HttpException('ValidationError', HttpStatus.UNPROCESSABLE_ENTITY);
    }
    return {
      token: await this.authService.tokenSign({user}),
      user,
    };
  }


  @Post('register')
  @ApiCreatedResponse({type: UserRegisterResponseDto})
  async register(@Body() data: UserRegisterRequestDto): Promise<UserRegisterResponseDto> {
    const user = await this.userService.create(data);
    // TODO handle errors
    return {
      user,
    };
  }

  @Get()
  @Roles(UserRole.ADMIN)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  getUser(@User() user) {
    return user;
  }

  @Get(':id')
  @ApiImplicitParam({name: 'id', type: Number})
  getUserById(@Param('id', UserByIdPipe) user: UserModel) {
    
    return user;
  }
}
// przykladowy token
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJuYW1lIjoiUGlvdHIiLCJlbWFpbCI6InBpb3RyQG15Zmxvdy5wbCIsInBhc3N3b3JkIjoiMTIzIiwicm9sZXMiOlsiYWRtaW4iXX0sImlhdCI6MTU2MDI0MTI3OH0.gLfzpqv-N8jJ_yTBjCYfe-T2EpSXT-BdcYdj1L3JiNM