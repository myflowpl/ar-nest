import { UserByIdPipe } from './user-by-id.pipe';
import { UserService } from '../services';
import { NotFoundException } from '@nestjs/common';

describe('UserByIdPipe', () => {
  const user = {
    id: 1,
    name: 'Piotr',
  };

  const userServiceMock: Partial<UserService> = {
    async getById(id: number) {
      if (id !== 1) {
        console.log('id', id);
        return null;
      }
      return user;
    },
  };
  let pipe: UserByIdPipe;

  beforeEach(() => {
    pipe = new UserByIdPipe(userServiceMock as UserService);
  });

  it('should be defined', () => {
    expect(new UserByIdPipe(userServiceMock as UserService)).toBeDefined();
  });

  it('should throw NotFoundException', async () => {
    expect(await pipe.transform('1')).toMatchObject(user);
  });

  it('should return null', () => {
    expect(pipe.transform('2')).rejects.toThrow(NotFoundException);
  });
});
